import { useState } from 'react'

function App() {
	const [num1, setNum1] = useState<string>('')
	const [num2, setNum2] = useState<string>('')
	const [result, setResult] = useState<number | null>(null)

	const performOperation = (action: string) => {
		const num1Val = +num1
		const num2Val = +num2

		if (isNaN(num1Val) || isNaN(num2Val)) {
			alert('Введите числовое значение')
			return
		}

		switch (action) {
			case 'plus':
				setResult(num1Val + num2Val)
				break
			case 'minus':
				setResult(num1Val - num2Val)
				break
			case 'mult':
				setResult(num1Val * num2Val)
				break
			case 'division':
				setResult(num1Val / num2Val)
				break
			default:
				setResult(0)
		}
	}

	const handleButtonClick = (operation: string) => {
		performOperation(operation)
	}

	const handleReset = () => {
		setNum1('')
		setNum2('')
		setResult(null)
	}

	return (
		<div className='container'>
			<div className='text-center'>
				<h1 className='text-black text-[32px] font-bold pt-10'>
					Blue calculator
				</h1>
				<div className='flex flex-col gap-3 mt-8 items-center'>
					<input
						type='text'
						id='num1'
						value={num1}
						onChange={e => setNum1(e.target.value)}
						placeholder='Num'
					/>
					<input
						type='text'
						id='num2'
						value={num2}
						onChange={e => setNum2(e.target.value)}
						placeholder='Num'
					/>
					<input
						type='text'
						id='res'
						value={result !== null ? result : ''}
						readOnly
						placeholder='Result'
					/>
				</div>
				<div className='flex mt-8 justify-center gap-4'>
					<button
						id='plus'
						className='btn bg-white transition hover:transition hover:-translate-y-2 '
						onClick={() => handleButtonClick('plus')}
					>
						+
					</button>
					<button
						id='minus'
						className='btn bg-white transition hover:transition hover:-translate-y-2 '
						onClick={() => handleButtonClick('minus')}
					>
						-
					</button>
					<button
						id='mult'
						className='btn bg-white transition hover:transition hover:-translate-y-2 '
						onClick={() => handleButtonClick('mult')}
					>
						*
					</button>
					<button
						id='division'
						className='btn bg-white transition hover:transition hover:-translate-y-2 '
						onClick={() => handleButtonClick('division')}
					>
						/
					</button>

					<button
						onClick={handleReset}
						title='Сброс данных'
						id='reset'
						className='btn bg-red-400 transition hover:-translate-y-2 hover:shadow-xl'
					>
						C
					</button>
				</div>
				{/* TODO: Разобраться как сделать так, чтобы результат появлялся только при
				нажатии на кнопку equals. */}
				{/* <button
					onClick={() => handleButtonClick('equals')}
					id='equals'
					className='w-32 mt-7 rounded-xl bg-[#1A79D1] text-white transition hover:bg-gray-700 hover:-translate-y-2 hover:shadow-xl'
				>
					=
				</button> */}
			</div>
		</div>
	)
}

export default App
